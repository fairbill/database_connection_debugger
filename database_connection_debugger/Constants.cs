﻿namespace database_connection_debugger
{
    class Constants
    {
        internal const string eaglesoft = "eaglesoft";
        internal const string error_occured = "An Error Occurred: ";
        internal const string dentrix = "dentrix version G5 - G6.1";
        internal const string opendental = "opendental";
        internal const string practiceweb = "practiceweb";
        internal const string crystal = "crystal";
        internal const string windent = "windent";
        internal const string easydental = "easydental";
        internal const string host = "host=";
        internal const string officemate = "officemate";
        internal const string chirotouch = "chirotouch";
        internal const string chiro8000 = "chiro8000";
        internal const string dentrix_version_G6_2_and_above = "dentrix version G6.2 and above";
        internal const string app_settings = "appSettings";
        internal const string server = "server";
        internal const string database = "database";
        internal const string connection_string = "connection_string";
        internal const string select_pms = "Select PMS";
    }
}
