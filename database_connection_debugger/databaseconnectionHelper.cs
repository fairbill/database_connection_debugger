﻿using Advantage.Data.Provider;
using Dtx.Persist.Core.Providers.CTreeThunker;
using Microsoft.Win32;
using MySql.Data.MySqlClient;
using Pervasive.Data.SqlClient;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Odbc;
using System.Data.SqlClient;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Text;
using System.Windows.Forms;

namespace database_connection_debugger
{
    class DatabaseConnectionHelper
    {
        #region db fields
        internal string db_userid = "";
        internal string db_password = "";
        internal string error = "";
        #endregion

        #region Dll Imports
        private const string DtxAPI = "Dentrix.API.dll";
        [DllImport(DtxAPI, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.StdCall)]
        internal static extern float DENTRIXAPI_GetDentrixVersion(); //G5.1+ available
        #endregion

        #region DDP member credentials - replace with your credentials
        private const string userId = "SPCPvsVn";
        private const string password = "M2RwP9buR";
        private string keyFilePath = Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), "dlls\\SPCPvsVn.dtxkey");
        #endregion

        #region private fields
        string doctible_installation_path = Directory.GetParent(Directory.GetCurrentDirectory()).FullName;
        RegistryKey regKey = Registry.LocalMachine.OpenSubKey(@"Software\Microsoft\Windows\CurrentVersion\App Paths");
        RegistryKey regKeyInWow6432Node = Registry.LocalMachine.OpenSubKey(@"SOFTWARE\Wow6432Node\Microsoft\Windows\CurrentVersion\Uninstall");
        string[] possible_eaglesoft_names = new string[] { "Patterson Eaglesoft" };
        #endregion

        internal string GetDefaultConnectionString(string source, string server, string database)
        {
            string connection_string = string.Empty;
            try
            {
                db_userid = string.Empty;
                db_password = string.Empty;
                switch (source)
                {
                    case Constants.officemate:
                        connection_string = "Server=" + server + ";Database=" + database + ";User Id=sa; Password=OMateSQL@2007;";
                        this.db_userid = "sa";
                        this.db_password = "OMateSQL@2007";
                        break;
                    case Constants.chirotouch:
                        connection_string = "Server=" + server + ";Database=" + database + ";User Id=sa; Password=Chir0touch;";
                        this.db_userid = "sa";
                        this.db_password = "Chir0touch";
                        break;
                    case Constants.chiro8000:
                        connection_string = "Server=" + server + ";Database=" + database + ";User Id=sa; Password=forte_sqlexpress;";
                        this.db_userid = "sa";
                        this.db_password = "forte_sqlexpress";
                        break;
                    case Constants.eaglesoft:
                        this.db_userid = "dba";
                        this.db_password = GetEaglesoftPassword(false);
                        connection_string = "DBN=" + database + ";DSN=" + server + ";UID=dba;PWD=" + this.db_password;
                        break;
                    case Constants.dentrix:
                        if (((int)(DENTRIXAPI_GetDentrixVersion() * 100) >= 1620) == true)
                        {
                            connection_string = string.Empty;
                            error = "Installed dentrix's version is greater than G6.1. Please select the option, 'dentrix version G6.2 and above' from the dropdown";
                        }
                        else
                        {
                            connection_string = GetDentrixConnectionString(server, database, userId, password, "");
                            this.db_userid = userId;
                            this.db_password = password;
                        }
                        break;
                    case Constants.dentrix_version_G6_2_and_above:
                        if (((int)(DENTRIXAPI_GetDentrixVersion() * 100) < 1620) == true)
                        {
                            connection_string = string.Empty;
                            error = "Installed dentrix's version is less than G6.1. Please select the option, 'dentrix version G5 - G6.1' from the dropdown";
                        }
                        else
                        {
                            connection_string = GetConnectionStringFromConfigFile();
                            if (connection_string != null && connection_string.Equals(string.Empty))
                            {
                                connection_string = "There is no default connection string for dentrix version G6.2 and higher";
                            }
                        }
                        break;
                    case Constants.opendental:
                    case Constants.practiceweb:
                        connection_string = "Server=" + server + ";Database = " + database + ";UID=root;PWD=";
                        this.db_userid = "root";
                        break;
                    case Constants.crystal:
                        connection_string = "Server=" + server + ";Database = " + database + ";UID=root;PWD=CNIrules";
                        this.db_userid = "root";
                        this.db_password = "CNIrules";
                        break;
                    case Constants.windent:
                        connection_string = "Server Name=" + server + ";Database Name=" + database + ";User ID=Master";
                        this.db_userid = "Master";
                        break;
                    case Constants.easydental:
                        connection_string = GetEasyDentalConnectionString();
                        break;
                    default:
                        connection_string = "Server=" + server + ";Database=" + database + ";User Id=sa; Password=pwd;";
                        this.db_userid = "sa";
                        this.db_password = "pwd";
                        break;
                }
            }
            catch (Exception ex)
            {
                this.error = "Exception in GetDefaultConnectionString method: " + ex.Message;
            }
            return connection_string;
        }

        internal string GetConnectionStringForDBManipulation(string source, string server, string database, string db_connection_string)
        {
            string connection_string = string.Empty;
            try
            {
                db_userid = string.Empty;
                db_password = string.Empty;
                switch (source)
                {
                    case Constants.eaglesoft:
                        if (GetEaglesoftVersion().Contains("17.00"))
                            connection_string = "DBN=" + database + ";DSN=" + server + ";UID=PDBA;PWD=4561676C65536F6674";
                        else
                            connection_string = "DBN=" + database + ";DSN=" + server + ";UID=" + GetEaglesoftUserId() + "; PWD=" + GetEaglesoftPassword(true);
                        break;
                    default:
                        connection_string = db_connection_string;
                        break;
                }
            }
            catch (Exception ex)
            {
                this.error = "Exception in GetConnectionStringForDBManipulation method: " + ex.Message;
            }
            return connection_string;
        }

        private string GetEasyDentalConnectionString()
        {
            string connection_string = string.Empty;
            try
            {
                GetPublicTypesFromDll(out Type[] publicTypes);
                connection_string = GetConnectionStringFromDLL(publicTypes);
            }
            catch (Exception ex)
            {
                this.error = "Exception in GetEasyDentalConnectionString methods: " + ex.Message;
            }
            return connection_string;
        }

        private void GetPublicTypesFromDll(out Type[] publicClasses)
        {
            Assembly assembly = Assembly.LoadFile(GetEasyDentalPathFromWindowsRegistry() + @"\DtxDBThnk.dll");
            publicClasses = assembly.GetExportedTypes();
        }

        private string GetConnectionStringFromDLL(Type[] publicClasses)
        {
            string connection_string = string.Empty;
            try
            {
                foreach (Type publicClass in publicClasses)
                {
                    if (publicClass != null && publicClass.AssemblyQualifiedName.Contains("DxConnect"))
                    {
                        FieldInfo[] staticFieldsList = publicClass.GetFields(BindingFlags.Static | BindingFlags.Public);
                        foreach (FieldInfo staticField in staticFieldsList)
                        {
                            if (staticField.Name.Equals("ConnectionString"))
                            {
                                connection_string = staticField.GetValue(null).ToString();
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                this.error = "Exception in GetConnectionStringFromDLL method: " + ex.Message;
            }
            return connection_string;
        }

        private string GetEasyDentalPathFromWindowsRegistry()
        {
            string easyDentalPath = string.Empty;

            GetInstalledAppList(out string[] installedAppList);

            for (int i = 0; i < installedAppList.Length; i++)
            {
                easyDentalPath = GetEasyDentalPath(installedAppList[i]);
            }

            if (easyDentalPath.Equals(string.Empty))
            {
                easyDentalPath = GetEasyDentalDefaultPath();
            }

            return easyDentalPath;
        }

        private string GetEasyDentalDefaultPath()
        {
            return @"C:\Program Files (x86)\EzDental\";
        }

        private string GetEasyDentalPath(string installedApp)
        {
            string easyDentalPath = string.Empty;
            try
            {
                RegistryKey subRegKey = regKey.OpenSubKey(installedApp);

                if (installedApp.Equals("Ezdental.exe") || installedApp.ToLower().Contains("ezdental"))
                {
                    easyDentalPath = subRegKey.GetValue("Path").ToString();
                }
            }
            catch (Exception ex)
            {
                this.error = "Exception in GetEasyDentalPath method: " + ex.Message;
                easyDentalPath = @"C:\Program Files (x86)\EzDental\";
            }
            return easyDentalPath;
        }

        private void GetInstalledAppList(out string[] installedAppList)
        {
            installedAppList = regKey.GetSubKeyNames(); ;
        }

        private string GetEaglesoftUserId()
        {
            string text = File.ReadAllText(doctible_installation_path + @"\EaglesoftPassword.txt");
            return text.Substring(text.LastIndexOf("PrimaryDbUserId = ") + 18, text.IndexOf("PrimaryDbPassword") - (text.LastIndexOf("PrimaryDbUserId = ") + 18)).Trim();
        }

        private string GetConnectionStringFromConfigFile()
        {
            string connection_string = string.Empty;
            try
            {
                Configuration config = GetConfiguration();
                connection_string = config.AppSettings.Settings[Constants.connection_string]?.Value?.ToString();
                SaveConfigurationSettings(config);
            }
            catch (Exception ex)
            {
                this.error = "Exception in GetConnectionStringFromConfigFile method: " + ex.Message;
            }
            return connection_string;
        }

        private void CallPattersonServerStatusApplication()
        {
            try
            {
                ProcessStartInfo processStartInfo = new ProcessStartInfo(Path.GetDirectoryName(Assembly.GetEntryAssembly().Location) + @"\dlls\es_encryption\PattersonServerStatus.exe");
                processStartInfo.Verb = "runas";
                var process = Process.Start(processStartInfo);
                process.WaitForExit();
            }
            catch (Exception ex)
            {
                this.error = "Exception in CallPattersonServerStatusApplication method: " + ex.Message;
            }
        }

        internal string GetCustomConnectionString(string source, string server, string database, string userid, string password, string db_connection_string)
        {
            string connection_string = string.Empty;
            switch (source)
            {
                case Constants.eaglesoft:
                    connection_string = "DBN=" + database + ";DSN=" + server + ";UID=" + userid + ";PWD=" + password;
                    break;
                case Constants.dentrix:
                case Constants.dentrix_version_G6_2_and_above:
                    connection_string = GetDentrixConnectionString(server, database, userid, password, db_connection_string);
                    break;
                case Constants.opendental:
                case Constants.practiceweb:
                case Constants.crystal:
                    connection_string = "Server=" + server + ";Database=" + database + ";UID=" + userid + ";PWD=" + password + ";";
                    break;
                case Constants.windent:
                    connection_string = "Server Name=" + server + ";Database Name=" + database + ";User ID=" + userid + ";Password=" + password + ";";
                    break;
                case Constants.easydental:
                    connection_string = db_connection_string + ";";
                    break;
                default:
                    connection_string = "Server=" + server + ";Database=" + database + ";User Id=" + userid + "; Password=" + password + ";";
                    break;
            }
            return connection_string;
        }

        internal string GetDentrixConnectionString(string server, string database, string userid, string password, string db_connection_string)
        {
            int connectionStringSize = 512;
            StringBuilder connectionString = new StringBuilder(connectionStringSize);
            string retStr = string.Empty;
            int temp = -1;
            int dentrixversion = (int)(DENTRIXAPI_GetDentrixVersion() * 100);
            try
            {
                lock (connectionString)
                {
                    //Make certain we are G6.2 or higher
                    retStr = ((int)(DENTRIXAPI_GetDentrixVersion() * 100) >= 1620) == true ? db_connection_string : Constants.host + server + ";UID=" + userid + ";PWD=" + password + "; Database=" + database + ";DSN=c-treeACE ODBC Driver; Connection Timeout=180";
                }
            }
            catch (Exception ex)
            {
                this.error = Constants.error_occured + "Error Occurred Obtaining Dentrix Connection String for " + ex.Message + " Register Value= " + temp.ToString() + " Dentrix Config: " + password;
            }
            return retStr;
        }

        internal bool CheckDBConn(string connectionString, string source)
        {
            try
            {
                bool isConnectionPossible = false;
                switch (source)
                {
                    case Constants.eaglesoft:
                    case Constants.dentrix:
                    case Constants.dentrix_version_G6_2_and_above:
                        isConnectionPossible = CheckDBConnectionWithODBCConnection(connectionString);
                        break;
                    case Constants.opendental:
                    case Constants.practiceweb:
                    case Constants.crystal:
                        isConnectionPossible = CheckDBConnectionWithMySqlConnection(connectionString);
                        break;
                    case Constants.windent:
                        isConnectionPossible = CheckDBConnectionWithPsqlConnection(connectionString);
                        break;
                    case Constants.easydental:
                        isConnectionPossible = CheckDBConnectionWithCTreeConnection(connectionString);
                        break;
                    default:
                        isConnectionPossible = CheckDBConnectionWithSqlConnection(connectionString);
                        break;
                }
                return isConnectionPossible;
            }
            catch (Exception e)
            {
                this.error = Constants.error_occured + "in CheckDBConn method: " + e.Message;
                return false;
            }
        }

        internal bool CheckDBConnectionWithSqlConnection(string connectionString)
        {
            using (var connection = new SqlConnection(connectionString))
            {
                connection.Open();
                connection.Close();
                return true;
            }
        }

        internal bool CheckDBConnectionWithCTreeConnection(string connectionString)
        {
            using (IDbConnection connection = new CTreeConnection(connectionString))
            {
                connection.Open();
                connection.Close();
                return true;
            }
        }

        internal bool CheckDBConnectionWithAdsConnection(string connectionString)
        {
            using (AdsConnection connection = new AdsConnection(connectionString))
            {
                connection.Open();
                connection.Close();
                return true;
            }
        }

        internal bool CheckDBConnectionWithPsqlConnection(string connectionString)
        {
            using (PsqlConnection connection = new PsqlConnection(connectionString))
            {
                connection.Open();
                connection.Close();
                return true;
            }
        }

        internal bool CheckDBConnectionWithMySqlConnection(string connectionString)
        {
            using (MySqlConnection connection = new MySqlConnection(connectionString))
            {
                connection.Open();
                connection.Close();
                return true;
            }
        }

        internal bool CheckDBConnectionWithODBCConnection(string connectionString)
        {
            using (OdbcConnection connection = new OdbcConnection(connectionString))
            {
                connection.ConnectionTimeout = 180;
                connection.Open();
                connection.Close();
                return true;
            }
        }

        internal void textChangedAction(TextBox textBox, string value)
        {
            if (textBox.Text.Equals(value))
            {
                textBox.Text = string.Empty;
            }
            else if (textBox.Text.Equals(string.Empty))
            {
                textBox.Text = value;
            }
        }

        internal Configuration GetConfiguration()
        {
            ExeConfigurationFileMap map = new ExeConfigurationFileMap { ExeConfigFilename = GetDoctibleServicePath() + @"\DoctibleService.exe.config" };
            return ConfigurationManager.OpenMappedExeConfiguration(map, ConfigurationUserLevel.None);
        }

        private string GetDoctibleServicePath()
        {
            string doctible_service_path = Path.GetDirectoryName(Assembly.GetEntryAssembly().Location);
            doctible_service_path = doctible_service_path.Substring(0, doctible_service_path.IndexOf("Doctible") + 8);
            return doctible_service_path;
        }

        internal void SaveConfigurationSettings(Configuration config)
        {
            config.Save(ConfigurationSaveMode.Modified);
            ConfigurationManager.RefreshSection(Constants.app_settings);
        }

        private string GetEaglesoftPassword(bool forDBManipulation)
        {
            string eaglesoft_password = string.Empty;
            try
            {
                if (GetEaglesoftVersion().Contains("17.00"))
                    eaglesoft_password = "sql";
                else
                {
                    CallPattersonServerStatusApplication();
                    string text = File.ReadAllText(doctible_installation_path + @"\EaglesoftPassword.txt");
                    if (forDBManipulation)
                    {
                        eaglesoft_password = text.Substring(text.LastIndexOf("PrimaryDbPassword = ") + 20, text.IndexOf("AssistantReadOnlyDatabaseUser") - (text.LastIndexOf("PrimaryDbPassword = ") + 20));
                    }
                    else
                    {
                        eaglesoft_password = text.Substring(text.LastIndexOf("PrimaryReadOnlyDatabasePassword = ") + 34, text.Length - (text.LastIndexOf("PrimaryReadOnlyDatabasePassword = ") + 34));
                    }
                }
            }
            catch (Exception ex)
            {
                this.error = "Exception in GetEaglesoftPassword method: " + ex.Message;
            }
            return eaglesoft_password.Trim();
        }

        internal DataTable GetDataUsingSQLConnection(string connection_string, string query, DataTable query_result)
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(connection_string))
                {
                    query_result = SQL_DBRead(connection, query, query_result);
                }
            }
            catch (Exception ex)
            {
                this.error = "Exception in GetDataUsingSQLConnection method: " + ex.Message;
            }
            return query_result;
        }

        internal DataTable GetDataUsingEasyDentalCTreeConnection(string connection_string, string query, DataTable query_result)
        {
            try
            {
                using (IDbConnection connection = new CTreeConnection(connection_string))
                {
                    query_result = EzDCTree_DBRead(connection, query, query_result);
                }
            }
            catch (Exception ex)
            {
                this.error = "Exception in GetDataUsingEasyDentalCTreeConnection method: " + ex.Message;
            }
            return query_result;
        }

        internal DataTable GetDataUsingPervasiveSQLConnection(string connection_string, string query, DataTable query_result)
        {
            try
            {
                using (PsqlConnection connection = new PsqlConnection(connection_string))
                {
                    query_result = PSQL_DBRead(connection, query, query_result);
                }
            }
            catch (Exception ex)
            {
                this.error = "Exception in GetDataUsingPervasiveSQLConnection method: " + ex.Message;
            }
            return query_result;
        }

        internal DataTable GetDataUsingMySQLConnection(string connection_string, string query, DataTable query_result)
        {
            try
            {
                using (MySqlConnection connection = new MySqlConnection(connection_string))
                {
                    query_result = MySQL_DBRead(connection, query, query_result);
                }
            }
            catch (Exception ex)
            {
                this.error = "Exception in GetDataUsingMySQLConnection method: " + ex.Message;
            }
            return query_result;
        }

        internal DataTable GetDataUsingODBCConnection(string connection_string, string query, DataTable query_result)
        {
            try
            {
                using (OdbcConnection connection = new OdbcConnection(connection_string))
                {
                    query_result = ODBC_DBRead(connection, query, query_result);
                }
            }
            catch (Exception ex)
            {
                this.error = "Exception in GetDataUsingODBCConnection method: " + ex.Message;
            }
            return query_result;
        }

        private DataTable SQL_DBRead(SqlConnection conn, string query, DataTable query_result)
        {
            try
            {
                using (SqlCommand cmd = new SqlCommand(query, conn))
                {
                    conn.Open();
                    query_result.Load(cmd.ExecuteReader());
                    conn.Close();
                }
            }
            catch (Exception ex)
            {
                this.error = ex.Message;
            }
            return query_result;
        }

        private DataTable EzDCTree_DBRead(IDbConnection conn, string query, DataTable query_result)
        {
            try
            {
                using (IDbCommand dbCommand = new CTreeCommand())
                {
                    dbCommand.CommandText = query;
                    dbCommand.Connection = (CTreeConnection)conn;
                    dbCommand.Connection.Open();
                    query_result.Load(dbCommand.ExecuteReader());
                    dbCommand.Connection.Close();
                }
            }
            catch (Exception ex)
            {
                this.error = ex.Message;
            }
            return query_result;
        }

        private DataTable PSQL_DBRead(PsqlConnection conn, string query, DataTable query_result)
        {
            try
            {
                using (PsqlCommand cmd = new PsqlCommand(query, conn))
                {
                    conn.Open();
                    query_result.Load(cmd.ExecuteReader());
                    conn.Close();
                }
            }
            catch (Exception ex)
            {
                this.error = ex.Message;
            }
            return query_result;
        }

        private DataTable MySQL_DBRead(MySqlConnection conn, string query, DataTable query_result)
        {
            try
            {
                using (MySqlCommand cmd = new MySqlCommand(query, conn))
                {
                    conn.Open();
                    query_result.Load(cmd.ExecuteReader());
                    conn.Close();
                }
            }
            catch (Exception ex)
            {
                this.error = ex.Message;
            }
            return query_result;
        }

        private DataTable ODBC_DBRead(OdbcConnection conn, string query, DataTable query_result)
        {
            try
            {
                using (OdbcCommand cmd = new OdbcCommand(query, conn))
                {
                    cmd.CommandTimeout = 180;
                    DataSet ds = new DataSet
                    {
                        EnforceConstraints = false
                    };
                    ds.Tables.Add(query_result);


                    conn.Open();
                    query_result.Load(cmd.ExecuteReader());
                    conn.Close();
                }
            }
            catch (Exception ex)
            {
                this.error = ex.Message;
            }
            return query_result;
        }

        internal int UpdateDBUsingSQLConnection(string connection_string, string query)
        {
            int query_result = -1;
            try
            {
                using (SqlConnection connection = new SqlConnection(connection_string))
                {
                    query_result = SQL_DBManipulationCommand(connection, query);
                }
            }
            catch (Exception ex)
            {
                this.error = "Exception in UpdateDBUsingSQLConnection method: " + ex.Message;
            }
            return query_result;
        }

        internal int UpdateDBUsingEasyDentalCTreeConnection(string connection_string, string query)
        {
            int query_result = -1;
            try
            {
                using (IDbConnection connection = new CTreeConnection(connection_string))
                {
                    query_result = EzDCTree_DBManipulationCommand(connection, query);
                }
            }
            catch (Exception ex)
            {
                this.error = "Exception in UpdateDBUsingEasyDentalCTreeConnection method: " + ex.Message;
            }
            return query_result;
        }

        internal int UpdateDBUsingPervasiveSQLConnection(string connection_string, string query)
        {
            int query_result = -1;
            try
            {
                using (PsqlConnection connection = new PsqlConnection(connection_string))
                {
                    query_result = PSQL_DBManipulationCommand(connection, query);
                }
            }
            catch (Exception ex)
            {
                this.error = "Exception in UpdateDBUsingPervasiveSQLConnection method: " + ex.Message;
            }
            return query_result;
        }

        internal int UpdateDBUsingMySQLConnection(string connection_string, string query)
        {
            int query_result = -1;
            try
            {
                using (MySqlConnection connection = new MySqlConnection(connection_string))
                {
                    query_result = MySQL_DBManipulationCommand(connection, query);
                }
            }
            catch (Exception ex)
            {
                this.error = "Exception in UpdateDBUsingMySQLConnection method: " + ex.Message;
            }
            return query_result;
        }

        internal int UpdateDBUsingODBCConnection(string connection_string, string query)
        {
            int query_result = -1;
            try
            {
                using (OdbcConnection connection = new OdbcConnection(connection_string))
                {
                    query_result = ODBC_DBManipulationCommand(connection, query);
                }
            }
            catch (Exception ex)
            {
                this.error = "Exception in UpdateDBUsingODBCConnection method: " + ex.Message;
            }
            return query_result;
        }

        private int SQL_DBManipulationCommand(SqlConnection connection, string query)
        {
            int query_result = -1;
            try
            {
                using (SqlCommand cmd = new SqlCommand(query, connection))
                {
                    connection.Open();
                    query_result = cmd.ExecuteNonQuery();
                    connection.Close();
                }
            }
            catch (Exception ex)
            {
                this.error = "Exception in SQL_DBManipulationCommand method: " + ex.Message;
            }
            return query_result;
        }

        private int EzDCTree_DBManipulationCommand(IDbConnection connection, string query)
        {
            int query_result = -1;
            try
            {
                using (IDbCommand dbCommand = new CTreeCommand())
                {
                    dbCommand.CommandText = query;
                    dbCommand.Connection = (CTreeConnection)connection;
                    dbCommand.Connection.Open();
                    query_result = dbCommand.ExecuteNonQuery();
                    dbCommand.Connection.Close();
                }
            }
            catch (Exception ex)
            {
                this.error = "Exception in EzDCTree_DBManipulationCommand method: " + ex.Message;
            }
            return query_result;
        }

        private int PSQL_DBManipulationCommand(PsqlConnection connection, string query)
        {
            int query_result = -1;
            try
            {
                using (PsqlCommand cmd = new PsqlCommand(query, connection))
                {
                    connection.Open();
                    query_result = cmd.ExecuteNonQuery();
                    connection.Close();
                }
            }
            catch (Exception ex)
            {
                this.error = "Exception in PSQL_DBManipulationCommand method: " + ex.Message;
            }
            return query_result;
        }

        private int MySQL_DBManipulationCommand(MySqlConnection connection, string query)
        {
            int query_result = -1;
            try
            {
                using (MySqlCommand cmd = new MySqlCommand(query, connection))
                {
                    connection.Open();
                    query_result = cmd.ExecuteNonQuery();
                    connection.Close();
                }
            }
            catch (Exception ex)
            {
                this.error = "Exception in MySQL_DBManipulationCommand method: " + ex.Message;
            }
            return query_result;
        }

        private int ODBC_DBManipulationCommand(OdbcConnection connection, string query)
        {
            int query_result = -1;
            try
            {
                using (OdbcCommand cmd = new OdbcCommand(query, connection))
                {
                    cmd.CommandTimeout = 180;
                    connection.Open();
                    query_result = cmd.ExecuteNonQuery();
                    connection.Close();
                }
            }
            catch (Exception ex)
            {
                this.error = "Exception in ODBC_DBManipulationCommand method: " + ex.Message;
            }
            return query_result;
        }

        internal int RunDBManipulationCommands(string query, string connection_string, string source)
        {
            int query_result = -1;
            switch (source)
            {
                case Constants.eaglesoft:
                case Constants.dentrix_version_G6_2_and_above:
                    query_result = UpdateDBUsingODBCConnection(connection_string, query);
                    break;
                case Constants.opendental:
                case Constants.practiceweb:
                case Constants.crystal:
                    query_result = UpdateDBUsingMySQLConnection(connection_string, query);
                    break;
                case Constants.windent:
                    query_result = UpdateDBUsingPervasiveSQLConnection(connection_string, query);
                    break;
                case Constants.easydental:
                    query_result = UpdateDBUsingEasyDentalCTreeConnection(connection_string, query);
                    break;
                default:
                    query_result = UpdateDBUsingSQLConnection(connection_string, query);
                    break;
            }
            return query_result;
        }

        internal void RunSelectStatement(string query, string connection_string, DataTable query_result, string source)
        {
            switch (source)
            {
                case Constants.eaglesoft:
                case Constants.dentrix:
                case Constants.dentrix_version_G6_2_and_above:
                    GetDataUsingODBCConnection(connection_string, query, query_result);
                    break;
                case Constants.opendental:
                case Constants.practiceweb:
                case Constants.crystal:
                    GetDataUsingMySQLConnection(connection_string, query, query_result);
                    break;
                case Constants.windent:
                    GetDataUsingPervasiveSQLConnection(connection_string, query, query_result);
                    break;
                case Constants.easydental:
                    GetDataUsingEasyDentalCTreeConnection(connection_string, query, query_result);
                    break;
                default:
                    GetDataUsingSQLConnection(connection_string, query, query_result);
                    break;
            }
        }

        internal string GetApplicationPathFromWindowsRegistry()
        {
            string installed_version = string.Empty;
            try
            {
                foreach (string installedApplication in GetInstalledApplicationsList())
                {
                    installed_version = GetApplicationPathFromWow6432Node(installedApplication);
                    if (!installed_version.Equals(string.Empty))
                    {
                        break;
                    }
                }
            }
            catch (Exception ex)
            {
                this.error = "Exception in GetApplicationPathFromWindowsRegistry method: " + ex.Message;
            }
            return installed_version;
        }

        private string GetApplicationPathFromWow6432Node(string installedApplication)
        {
            string installed_version = string.Empty;
            try
            {
                //Get details of an installed application
                RegistryKey subRegKey = GetDetailsOfAnInstalledApplication(installedApplication, regKeyInWow6432Node);
                if (subRegKey?.GetValue("DisplayName") != null)
                {
                    string displayName = subRegKey.GetValue("DisplayName").ToString();
                    if (IsPMSInstalled(possible_eaglesoft_names, displayName))
                    {
                        installed_version = subRegKey.GetValue("DisplayVersion").ToString();
                    }
                }
            }
            catch (Exception ex)
            {
                this.error = "Exception in GetApplicationPathFromWow6432Node method: " + ex.Message;
            }
            return installed_version;
        }

        private bool IsPMSInstalled(string[] possible_pms_display_names_in_registry, string installed_pms_name)
        {
            bool IsPMSInstalled = false;
            try
            {
                foreach (string name in installed_pms_name.Split(' '))
                {
                    if (possible_pms_display_names_in_registry.Any(installed_pms_name.Contains) || possible_pms_display_names_in_registry.Any(name.Contains))
                    {
                        IsPMSInstalled = true;
                    }
                }
            }
            catch (Exception ex)
            {
                this.error = "Exception in IsPMSInstalled method: " + ex.Message;
            }
            return IsPMSInstalled;
        }

        private RegistryKey GetDetailsOfAnInstalledApplication(string installedApplication, RegistryKey registryKey)
        {
            RegistryKey subRegKey = null;
            try
            {
                subRegKey = registryKey.OpenSubKey(installedApplication);
            }
            catch (Exception ex)
            {
                this.error = "Exception in GetDetailsOfAnInstalledApplication method: " + ex.Message;
                subRegKey = null;
            }
            return subRegKey;
        }

        private List<string> GetInstalledApplicationsList()
        {
            List<string> installedApplicationsList = new List<string>();
            try
            {
                installedApplicationsList.AddRange(regKeyInWow6432Node.GetSubKeyNames());
            }
            catch (Exception ex)
            {
                this.error = "Exception in GetInstalledApplicationsList method: " + ex.Message;
            }
            return installedApplicationsList;
        }

        private string GetEaglesoftVersion()
        {
            string eaglesoft_version = string.Empty;
            try
            {
                eaglesoft_version = GetApplicationPathFromWindowsRegistry();
            }
            catch (Exception ex)
            {
                this.error = "Exception in GetEaglesoftVersion method: " + ex.Message;
            }
            return eaglesoft_version;
        }
    }
}
