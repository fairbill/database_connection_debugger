﻿using System;

namespace database_connection_debugger
{
    partial class FormSQLBrowser
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormSQLBrowser));
            this.cmbBoxSelectPMS = new System.Windows.Forms.ComboBox();
            this.txtConnectionString = new System.Windows.Forms.TextBox();
            this.lblConnectionString = new System.Windows.Forms.Label();
            this.lnkBack = new System.Windows.Forms.LinkLabel();
            this.txtEnterQuery = new System.Windows.Forms.RichTextBox();
            this.lblEnterQuery = new System.Windows.Forms.Label();
            this.btnRunSQL = new System.Windows.Forms.Button();
            this.lblErrorMessage = new System.Windows.Forms.Label();
            this.lblQueryResult = new System.Windows.Forms.Label();
            this.dataGridViewQueryResult = new System.Windows.Forms.DataGridView();
            this.chkBoxEditDBEntry = new System.Windows.Forms.CheckBox();
            this.lblWarning = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewQueryResult)).BeginInit();
            this.SuspendLayout();
            // 
            // cmbBoxSelectPMS
            // 
            this.cmbBoxSelectPMS.Enabled = false;
            this.cmbBoxSelectPMS.FormattingEnabled = true;
            this.cmbBoxSelectPMS.Items.AddRange(new object[] {
            "Select PMS",
            "chiro8000",
            "chirotouch",
            "crystal",
            "dentrix version G5 - G6.1",
            "dentrix version G6.2 and above",
            "eaglesoft",
            "easydental",
            "officemate",
            "opendental",
            "practiceweb",
            "windent"});
            this.cmbBoxSelectPMS.Location = new System.Drawing.Point(58, 102);
            this.cmbBoxSelectPMS.Name = "cmbBoxSelectPMS";
            this.cmbBoxSelectPMS.Size = new System.Drawing.Size(304, 33);
            this.cmbBoxSelectPMS.TabIndex = 1;
            // 
            // txtConnectionString
            // 
            this.txtConnectionString.Enabled = false;
            this.txtConnectionString.Location = new System.Drawing.Point(262, 160);
            this.txtConnectionString.Name = "txtConnectionString";
            this.txtConnectionString.Size = new System.Drawing.Size(823, 31);
            this.txtConnectionString.TabIndex = 3;
            // 
            // lblConnectionString
            // 
            this.lblConnectionString.AutoSize = true;
            this.lblConnectionString.Location = new System.Drawing.Point(61, 166);
            this.lblConnectionString.Name = "lblConnectionString";
            this.lblConnectionString.Size = new System.Drawing.Size(195, 25);
            this.lblConnectionString.TabIndex = 2;
            this.lblConnectionString.Text = "Connection String: ";
            // 
            // lnkBack
            // 
            this.lnkBack.AutoSize = true;
            this.lnkBack.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lnkBack.LinkColor = System.Drawing.Color.CornflowerBlue;
            this.lnkBack.Location = new System.Drawing.Point(53, 43);
            this.lnkBack.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.lnkBack.Name = "lnkBack";
            this.lnkBack.Size = new System.Drawing.Size(66, 29);
            this.lnkBack.TabIndex = 0;
            this.lnkBack.TabStop = true;
            this.lnkBack.Text = "Back";
            this.lnkBack.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.lnkBack_LinkClicked);
            // 
            // txtEnterQuery
            // 
            this.txtEnterQuery.Location = new System.Drawing.Point(262, 230);
            this.txtEnterQuery.Name = "txtEnterQuery";
            this.txtEnterQuery.Size = new System.Drawing.Size(823, 99);
            this.txtEnterQuery.TabIndex = 5;
            this.txtEnterQuery.Text = "";
            this.txtEnterQuery.Click += new System.EventHandler(this.txtEnterQuery_Click);
            // 
            // lblEnterQuery
            // 
            this.lblEnterQuery.AutoSize = true;
            this.lblEnterQuery.Location = new System.Drawing.Point(61, 282);
            this.lblEnterQuery.Name = "lblEnterQuery";
            this.lblEnterQuery.Size = new System.Drawing.Size(129, 25);
            this.lblEnterQuery.TabIndex = 4;
            this.lblEnterQuery.Text = "Enter query:";
            // 
            // btnRunSQL
            // 
            this.btnRunSQL.Location = new System.Drawing.Point(491, 405);
            this.btnRunSQL.Name = "btnRunSQL";
            this.btnRunSQL.Size = new System.Drawing.Size(291, 51);
            this.btnRunSQL.TabIndex = 8;
            this.btnRunSQL.Text = "Run SQL";
            this.btnRunSQL.UseVisualStyleBackColor = true;
            this.btnRunSQL.Click += new System.EventHandler(this.btnRunSQL_Click);
            // 
            // lblErrorMessage
            // 
            this.lblErrorMessage.Location = new System.Drawing.Point(58, 346);
            this.lblErrorMessage.Name = "lblErrorMessage";
            this.lblErrorMessage.Size = new System.Drawing.Size(1027, 56);
            this.lblErrorMessage.TabIndex = 9;
            this.lblErrorMessage.Text = "\'";
            this.lblErrorMessage.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lblErrorMessage.Visible = false;
            // 
            // lblQueryResult
            // 
            this.lblQueryResult.AutoSize = true;
            this.lblQueryResult.Location = new System.Drawing.Point(61, 555);
            this.lblQueryResult.Name = "lblQueryResult";
            this.lblQueryResult.Size = new System.Drawing.Size(143, 25);
            this.lblQueryResult.TabIndex = 10;
            this.lblQueryResult.Text = "Query Result:";
            this.lblQueryResult.Visible = false;
            // 
            // dataGridViewQueryResult
            // 
            this.dataGridViewQueryResult.AllowUserToAddRows = false;
            this.dataGridViewQueryResult.AllowUserToDeleteRows = false;
            this.dataGridViewQueryResult.AllowUserToOrderColumns = true;
            this.dataGridViewQueryResult.BackgroundColor = System.Drawing.Color.WhiteSmoke;
            this.dataGridViewQueryResult.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.dataGridViewQueryResult.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.dataGridViewQueryResult.ClipboardCopyMode = System.Windows.Forms.DataGridViewClipboardCopyMode.EnableAlwaysIncludeHeaderText;
            this.dataGridViewQueryResult.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.875F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.Color.Transparent;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.Color.Transparent;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewQueryResult.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dataGridViewQueryResult.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.875F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.Color.Transparent;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.Color.Transparent;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewQueryResult.DefaultCellStyle = dataGridViewCellStyle2;
            this.dataGridViewQueryResult.GridColor = System.Drawing.Color.White;
            this.dataGridViewQueryResult.Location = new System.Drawing.Point(262, 503);
            this.dataGridViewQueryResult.Name = "dataGridViewQueryResult";
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.Color.Transparent;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.875F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.Color.Transparent;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.Color.Transparent;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewQueryResult.RowHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.dataGridViewQueryResult.RowHeadersVisible = false;
            this.dataGridViewQueryResult.RowTemplate.Height = 18;
            this.dataGridViewQueryResult.RowTemplate.ReadOnly = true;
            this.dataGridViewQueryResult.Size = new System.Drawing.Size(823, 557);
            this.dataGridViewQueryResult.TabIndex = 12;
            this.dataGridViewQueryResult.Visible = false;
            this.dataGridViewQueryResult.DataError += new System.Windows.Forms.DataGridViewDataErrorEventHandler(this.dataGridViewQueryResult_DataError);
            // 
            // chkBoxEditDBEntry
            // 
            this.chkBoxEditDBEntry.AutoSize = true;
            this.chkBoxEditDBEntry.Location = new System.Drawing.Point(969, 102);
            this.chkBoxEditDBEntry.Name = "chkBoxEditDBEntry";
            this.chkBoxEditDBEntry.Size = new System.Drawing.Size(116, 29);
            this.chkBoxEditDBEntry.TabIndex = 13;
            this.chkBoxEditDBEntry.Text = "Edit DB";
            this.chkBoxEditDBEntry.UseVisualStyleBackColor = true;
            this.chkBoxEditDBEntry.CheckedChanged += new System.EventHandler(this.chkBoxEditDBEntry_CheckedChanged);
            // 
            // lblWarning
            // 
            this.lblWarning.AutoSize = true;
            this.lblWarning.ForeColor = System.Drawing.Color.Red;
            this.lblWarning.Location = new System.Drawing.Point(200, 47);
            this.lblWarning.Name = "lblWarning";
            this.lblWarning.Size = new System.Drawing.Size(930, 25);
            this.lblWarning.TabIndex = 19;
            this.lblWarning.Text = "Please don\'t use Edit DB option or execute DB manipulation queries for Eaglesoft " +
    "version < 17.0";
            // 
            // FormSQLBrowser
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(12F, 25F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1152, 1113);
            this.Controls.Add(this.lblWarning);
            this.Controls.Add(this.chkBoxEditDBEntry);
            this.Controls.Add(this.dataGridViewQueryResult);
            this.Controls.Add(this.lblQueryResult);
            this.Controls.Add(this.lblErrorMessage);
            this.Controls.Add(this.btnRunSQL);
            this.Controls.Add(this.txtEnterQuery);
            this.Controls.Add(this.lblEnterQuery);
            this.Controls.Add(this.lnkBack);
            this.Controls.Add(this.txtConnectionString);
            this.Controls.Add(this.lblConnectionString);
            this.Controls.Add(this.cmbBoxSelectPMS);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "FormSQLBrowser";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Database Connection Debugger";
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewQueryResult)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox cmbBoxSelectPMS;
        private System.Windows.Forms.TextBox txtConnectionString;
        private System.Windows.Forms.Label lblConnectionString;
        private System.Windows.Forms.LinkLabel lnkBack;
        private System.Windows.Forms.RichTextBox txtEnterQuery;
        private System.Windows.Forms.Label lblEnterQuery;
        private System.Windows.Forms.Button btnRunSQL;
        private System.Windows.Forms.Label lblErrorMessage;
        private System.Windows.Forms.Label lblQueryResult;
        private System.Windows.Forms.DataGridView dataGridViewQueryResult;
        private System.Windows.Forms.CheckBox chkBoxEditDBEntry;
        private System.Windows.Forms.Label lblWarning;
    }
}