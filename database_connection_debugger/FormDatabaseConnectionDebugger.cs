﻿using System;
using System.Drawing;
using System.Configuration;
using System.Windows.Forms;

namespace database_connection_debugger
{
    public partial class FormDatabaseConnectionDebugger : Form
    {
        #region variables
        int selectedIndex = 0;
        bool showSQLBrowser = false;
        string servername = string.Empty;
        string databasename = string.Empty;
        string selectedItem = string.Empty;
        DatabaseConnectionHelper hp = new DatabaseConnectionHelper();
        #endregion

        public FormDatabaseConnectionDebugger()
        {
            InitializeComponent();
        }

        public FormDatabaseConnectionDebugger(int selectedIndex, string servername, string databasename)
        {
            InitializeComponent();
            this.selectedIndex = selectedIndex;
            this.servername = servername;
            this.databasename = databasename;
            showSQLBrowser = true;
        }

        private void FormDatabaseConnectionDebugger_Load(object sender, EventArgs e)
        {
            SetComboBoxSettings();
            AlignButtonsToCenter();
            HideComponents();
            PopulateComponents();
            this.servername = string.Empty;
            this.databasename = string.Empty;
        }

        private void PopulateComponents()
        {
            if (this.selectedIndex != 0)
            {
                cmbBoxSelectPMS_SelectedIndexChangedAction();
                btnPopulateConnectionString_ClickAction();
                if (showSQLBrowser)
                {
                    this.lnkSQLBrowser.Show();
                }
            }
        }

        private void cmbBoxSelectPMS_SelectedIndexChanged(object sender, EventArgs e)
        {
            cmbBoxSelectPMS_SelectedIndexChangedAction();
        }

        private void cmbBoxSelectPMS_SelectedIndexChangedAction()
        {
            this.selectedItem = cmbBoxSelectPMS.Items[cmbBoxSelectPMS.SelectedIndex].ToString();
            ResetFields();
            HideComponents();
            this.btnPopulateConnectionString.Show();
            if (!this.selectedItem.Equals(Constants.easydental) && !this.selectedItem.Equals(Constants.dentrix_version_G6_2_and_above) && !this.selectedItem.Equals(Constants.select_pms))
            {
                ShowComponents();
                EnableComponents();
                if (this.selectedItem.Contains("dentrix"))
                {
                    this.txtDatabase.Text = "DentrixSQL";
                }
                PopulatePropertiesUsingLocalConfigFile();
            }
        }

        private void btnCheckDBConnection_Click(object sender, EventArgs e)
        {
            ShowMessage("Processing. Please wait...", Color.Black);
            if (ValidateFields())
            {
                bool success = hp.CheckDBConn(this.txtConnectionString.Text.Trim(), this.selectedItem);
                ShowMessage(success ? "Successfully connected to DB" : "Couldn't make a connection to DB. Please check the connection string", success ? Color.Green : Color.Red);
                if (success)
                {
                    this.lnkSQLBrowser.Show();
                }
            }
        }

        private void btnPopulateConnectionString_Click(object sender, EventArgs e)
        {
            btnPopulateConnectionString_ClickAction();
        }

        private void btnEditConnectionString_Click(object sender, EventArgs e)
        {
            this.btnEditConnectionString.Hide();
            this.lblMessage.Hide();
            this.lnkSQLBrowser.Hide();

            if (!this.selectedItem.Equals(Constants.easydental) && !this.selectedItem.Equals(Constants.dentrix_version_G6_2_and_above))
            {
                ShowUserIAndPassword();
                HideConnectionString();
                EnableComponents();
                this.lblMessage.Text = string.Empty;
                this.btnCheckDBConnection.Hide();
                this.btnPopulateConnectionString.Show();
                ShowComponents();
            }
            else
            {
                this.lblConnectionString.Show();
                this.txtConnectionString.Show();
            }
        }

        private void txtField_MouseClick(object sender, MouseEventArgs e)
        {
            this.lblMessage.Text = string.Empty;
        }

        private void txtServer_MouseDown(object sender, MouseEventArgs e)
        {
            hp.textChangedAction(this.txtServer, "servername");
        }

        private void txtDatabase_MouseDown(object sender, MouseEventArgs e)
        {
            hp.textChangedAction(this.txtDatabase, "dbname");
        }

        private void lnkSQLBrowser_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            this.Hide();
            FormSQLBrowser sqlBrowser = new FormSQLBrowser(this.cmbBoxSelectPMS.SelectedIndex, this.txtConnectionString.Text.Trim(), this.txtServer.Text.Trim(), this.txtDatabase.Text.Trim());
            sqlBrowser.ShowDialog();
            this.Close();
        }

        private void SetComboBoxSettings()
        {
            this.cmbBoxSelectPMS.SelectedIndex = this.selectedIndex;
            this.cmbBoxSelectPMS.AutoCompleteMode = AutoCompleteMode.SuggestAppend;
            this.cmbBoxSelectPMS.AutoCompleteSource = AutoCompleteSource.ListItems;
        }

        private void AlignButtonsToCenter()
        {
            this.btnCheckDBConnection.Left = (this.ClientSize.Width - this.btnCheckDBConnection.Width) / 2;
            this.btnPopulateConnectionString.Left = (this.ClientSize.Width - this.btnPopulateConnectionString.Width) / 2;
        }

        private void HideComponents()
        {
            HideServer();
            HideDatabase();
            HideUserIdAndPassword();
            HideConnectionString();
            HideButtons();
            this.lnkSQLBrowser.Hide();
        }

        private void HideButtons()
        {
            this.btnCheckDBConnection.Hide();
            this.btnPopulateConnectionString.Hide();
            this.btnEditConnectionString.Hide();
            this.lnkSQLBrowser.Hide();
        }

        private void HideUserIdAndPassword()
        {
            this.lblUserId.Hide();
            this.txtUserId.Hide();
            this.lblPassword.Hide();
            this.txtPassword.Hide();
        }

        private void HideDatabase()
        {
            this.lblDatabase.Hide();
            this.txtDatabase.Hide();
            this.lblStar2.Hide();
        }

        private void HideServer()
        {
            this.lblServer.Hide();
            this.txtServer.Hide();
            this.lblStar1.Hide();
        }

        private void EnableComponents()
        {
            this.txtServer.Enabled = true;
            this.txtDatabase.Enabled = true;
            this.txtUserId.Enabled = true;
            this.txtPassword.Enabled = true;
        }

        private void DisableComponents()
        {
            this.txtServer.Enabled = false;
            this.txtDatabase.Enabled = false;
            this.txtUserId.Enabled = false;
            this.txtPassword.Enabled = false;
        }

        private void ResetFields()
        {
            this.txtServer.Text = this.servername.Equals(string.Empty) ? string.Empty : this.servername;
            this.txtDatabase.Text = this.databasename.Equals(string.Empty) ? string.Empty : this.databasename;
            this.txtUserId.Text = string.Empty;
            this.txtPassword.Text = string.Empty;
            this.lblMessage.Text = string.Empty;
            this.txtConnectionString.Text = string.Empty;
            hp.db_userid = string.Empty;
            hp.db_password = string.Empty;
        }

        private void ShowComponents()
        {
            DisplayServer();
            DisplayDataBase();
        }

        private void DisplayDataBase()
        {
            this.lblStar2.Show();
            this.lblDatabase.Show();
            this.txtDatabase.Show();
        }

        private void DisplayServer()
        {
            this.lblServer.Show();
            this.txtServer.Show();
            this.lblStar1.Show();
        }

        private bool ValidateFields()
        {
            if (this.selectedItem.Equals(Constants.select_pms) && !this.selectedItem.Equals(string.Empty))
            {
                ShowMessage("Please select a PMS", Color.Red);
                return false;
            }
            else if (this.selectedItem.Equals(Constants.easydental) || this.selectedItem.Equals(Constants.dentrix_version_G6_2_and_above))
            {
                return true;
            }
            else
            {
                string message = this.txtServer.Text.Equals(string.Empty) ? "Please enter server name" : this.txtDatabase.Text.Equals(string.Empty) ? "Please enter database name" : string.Empty;
                if (message == string.Empty)
                {
                    return true;
                }
                else
                {
                    ShowMessage(message, Color.Red);
                    return false;
                }
            }
        }

        private void ShowMessage(string message, Color color)
        {
            this.lblMessage.Show();
            this.lblMessage.Text = message;
            this.lblMessage.ForeColor = color;
            this.lblMessage.Left = (this.ClientSize.Width - this.lblMessage.Width) / 2;
            this.Refresh();
        }

        private void GetConnectionString(out string connection_string)
        {
            if (this.txtUserId.Text.Equals(string.Empty) && this.txtPassword.Text == string.Empty)
            {
                connection_string = hp.GetDefaultConnectionString(this.selectedItem, this.txtServer.Text.Trim(), this.txtDatabase.Text.Trim());
                this.txtUserId.Text = hp.db_userid;
                this.txtPassword.Text = hp.db_password;
            }
            else
            {
                connection_string = hp.GetCustomConnectionString(this.selectedItem, this.txtServer.Text.Trim(), this.txtDatabase.Text.Trim(), this.txtUserId.Text.Trim(), this.txtPassword.Text.Trim(), this.txtConnectionString.Text.Trim());
            }

            if (hp.error != string.Empty)
            {
                ShowMessage(hp.error, Color.Red);
            }
        }

        private void btnPopulateConnectionString_ClickAction()
        {
            this.lblMessage.Text = string.Empty;
            hp.error = string.Empty;

            if (ValidateFields())
            {
                GetConnectionString(out string connectionstring);
                if (connectionstring != null && !connectionstring.Equals(string.Empty))
                {
                    this.txtConnectionString.Text = connectionstring;

                    if (!this.selectedItem.Equals(Constants.easydental) && !this.selectedItem.Equals(Constants.dentrix_version_G6_2_and_above))
                    {
                        ShowUserIAndPassword();
                    }
                    this.lblConnectionString.Show();
                    this.txtConnectionString.Show();
                    this.btnPopulateConnectionString.Hide();
                    this.btnCheckDBConnection.Show();
                    this.btnEditConnectionString.Show();
                    DisableComponents();
                }
                else
                {
                    this.lblConnectionString.Show();
                    this.txtConnectionString.Show();
                }
            }
        }

        private void ShowUserIAndPassword()
        {
            this.lblUserId.Show();
            this.txtUserId.Show();
            this.lblPassword.Show();
            this.txtPassword.Show();
        }

        private void HideConnectionString()
        {
            this.lblConnectionString.Hide();
            this.txtConnectionString.Hide();
        }

        private void PopulatePropertiesUsingLocalConfigFile()
        {
            Configuration config = hp.GetConfiguration();
            this.txtServer.Text = this.servername.Equals(string.Empty) ? config.AppSettings.Settings[Constants.server]?.Value?.ToString() : this.servername;
            this.txtDatabase.Text = this.databasename.Equals(string.Empty) ? config.AppSettings.Settings[Constants.database]?.Value?.ToString() : this.databasename;
            hp.SaveConfigurationSettings(config);
        }
    }
}
