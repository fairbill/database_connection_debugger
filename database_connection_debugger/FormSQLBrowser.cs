﻿using System;
using System.Data;
using System.Drawing;
using System.Windows.Forms;

namespace database_connection_debugger
{
    public partial class FormSQLBrowser : Form
    {
        #region private variables
        DatabaseConnectionHelper hp;
        string servername = string.Empty;
        string databasename = string.Empty;
        #endregion

        public FormSQLBrowser(int selectedItem, string connection_string, string servername, string databasename)
        {
            InitializeComponent();
            this.cmbBoxSelectPMS.SelectedIndex = selectedItem;
            if (this.cmbBoxSelectPMS.SelectedIndex == 6)
                this.lblWarning.Show();
            else
                this.lblWarning.Hide();
            this.txtConnectionString.Text = connection_string;
            this.servername = servername;
            this.databasename = databasename;
            hp = new DatabaseConnectionHelper();
            this.Size = new Size(620, 300);
        }

        private void lnkBack_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            this.Hide();
            FormDatabaseConnectionDebugger dbConnectionDebugger = new FormDatabaseConnectionDebugger(this.cmbBoxSelectPMS.SelectedIndex, this.servername, this.databasename);
            dbConnectionDebugger.ShowDialog();
            this.Close();
        }

        private void btnRunSQL_Click(object sender, EventArgs e)
        {
            hp.error = string.Empty;
            string query = this.txtEnterQuery.Text.Trim().ToLower();

            if (this.txtEnterQuery.Text.Trim().Equals(string.Empty))
            {
                ShowMessage("Please enter a valid query and try again.", Color.Red);
            }
            else if (!chkBoxEditDBEntry.Checked && query.IndexOf("select") != 0)
            {
                ShowMessage("Please select the checkbox, if you want to continue with the query.. ", Color.Red);
            }
            else
            {
                RunSQLQueryAndDisplayResults(query);
            }
        }

        private void RunSQLQueryAndDisplayResults(string query)
        {
            int db_query_result = -1;
            ShowMessage("Processing. Please wait.. ", Color.Black);
            DataTable query_result = new DataTable();

            if (query.IndexOf("select") == 0)
            {
                hp.RunSelectStatement(query, this.txtConnectionString.Text.Trim(), query_result, this.cmbBoxSelectPMS.SelectedItem.ToString());
            }
            else if (chkBoxEditDBEntry.Checked)
            {
                string connection_string = hp.GetConnectionStringForDBManipulation(this.cmbBoxSelectPMS.SelectedItem.ToString(), this.servername, this.databasename, this.txtConnectionString.Text);
                this.txtConnectionString.Text = connection_string;
                db_query_result = hp.RunDBManipulationCommands(query, connection_string.Trim(), this.cmbBoxSelectPMS.SelectedItem.ToString());
            }

            DisplayResults(query_result, db_query_result);
        }

        private void DisplayResults(DataTable query_result, int db_query_result)
        {
            if (hp.error.Equals(string.Empty) || db_query_result != -1)
            {
                DisplayQueryResult(query_result, db_query_result);
            }
            else
            {
                HideComponentsAndPositionRunSQLButton();
                ShowMessage(hp.error, Color.Red);
            }
        }

        private void HideComponentsAndPositionRunSQLButton()
        {
            this.lblQueryResult.Hide();
            this.dataGridViewQueryResult.Hide();
            this.Size = new Size(620, 300);
        }

        private void DisplayQueryResult(DataTable query_result, int db_query_result)
        {
            if (db_query_result != -1)
            {
                HideComponentsAndPositionRunSQLButton();
                ShowMessage(db_query_result + " row(s) affected.", Color.Green);
            }
            else if (query_result.Rows.Count == 0)
            {
                HideComponentsAndPositionRunSQLButton();
                ShowMessage("No data found.", Color.Black);
            }
            else
            {
                if (hp.error.Equals(string.Empty))
                {
                    DisplayQueryResultInDataGridView(query_result);
                }
                else
                {
                    ShowMessage(hp.error, Color.Red);
                }
            }
        }

        private void DisplayQueryResultInDataGridView(DataTable query_result)
        {
            this.dataGridViewQueryResult.DataSource = query_result;
            this.dataGridViewQueryResult.Show();
            this.dataGridViewQueryResult.ClearSelection();
            this.lblQueryResult.Show();
            this.lblErrorMessage.Hide();
            this.Size = new Size(630, 630);
        }

        private void ShowMessage(string message, Color color)
        {
            this.lblErrorMessage.Text = message;
            this.lblErrorMessage.ForeColor = color;
            this.lblErrorMessage.Left = (this.ClientSize.Width - this.lblErrorMessage.Width) / 2;
            this.lblErrorMessage.Show();
            this.Refresh();
        }

        private void txtEnterQuery_Click(object sender, EventArgs e)
        {
            this.lblErrorMessage.Text = string.Empty;
        }

        private void dataGridViewQueryResult_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {
            e.ThrowException = false;
            ShowMessage(e.Exception.Message, Color.Red);
        }

        private void chkBoxEditDBEntry_CheckedChanged(object sender, EventArgs e)
        {
            if (chkBoxEditDBEntry.Checked == true)
            {
                ShowDialogBox();
            }
        }

        private void ShowDialogBox()
        {
            DialogResult dialogResult = MessageBox.Show(@"Are you sure you want to edit database entries?", @"Edit Database entries", buttons: MessageBoxButtons.YesNo, icon: MessageBoxIcon.Warning);

            if (dialogResult.Equals(DialogResult.No))
            {
                chkBoxEditDBEntry.Checked = false;
            }
            this.lblErrorMessage.Text = string.Empty;
        }
    }
}
