﻿namespace database_connection_debugger
{
    partial class FormDatabaseConnectionDebugger
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormDatabaseConnectionDebugger));
            this.btnCheckDBConnection = new System.Windows.Forms.Button();
            this.cmbBoxSelectPMS = new System.Windows.Forms.ComboBox();
            this.lblConnectionString = new System.Windows.Forms.Label();
            this.txtConnectionString = new System.Windows.Forms.TextBox();
            this.lblServer = new System.Windows.Forms.Label();
            this.lblDatabase = new System.Windows.Forms.Label();
            this.lblUserId = new System.Windows.Forms.Label();
            this.lblPassword = new System.Windows.Forms.Label();
            this.txtServer = new System.Windows.Forms.TextBox();
            this.txtDatabase = new System.Windows.Forms.TextBox();
            this.txtUserId = new System.Windows.Forms.TextBox();
            this.txtPassword = new System.Windows.Forms.TextBox();
            this.lblMessage = new System.Windows.Forms.Label();
            this.btnPopulateConnectionString = new System.Windows.Forms.Button();
            this.lblStar1 = new System.Windows.Forms.Label();
            this.lblStar2 = new System.Windows.Forms.Label();
            this.btnEditConnectionString = new System.Windows.Forms.Button();
            this.lnkSQLBrowser = new System.Windows.Forms.LinkLabel();
            this.SuspendLayout();
            // 
            // btnCheckDBConnection
            // 
            this.btnCheckDBConnection.Location = new System.Drawing.Point(425, 618);
            this.btnCheckDBConnection.Name = "btnCheckDBConnection";
            this.btnCheckDBConnection.Size = new System.Drawing.Size(319, 51);
            this.btnCheckDBConnection.TabIndex = 14;
            this.btnCheckDBConnection.Text = "Check Database Connection";
            this.btnCheckDBConnection.UseVisualStyleBackColor = true;
            this.btnCheckDBConnection.Click += new System.EventHandler(this.btnCheckDBConnection_Click);
            // 
            // cmbBoxSelectPMS
            // 
            this.cmbBoxSelectPMS.FormattingEnabled = true;
            this.cmbBoxSelectPMS.Items.AddRange(new object[] {
            "Select PMS",
            "chiro8000",
            "chirotouch",
            "crystal",
            "dentrix version G5 - G6.1",
            "dentrix version G6.2 and above",
            "eaglesoft",
            "easydental",
            "officemate",
            "opendental",
            "practiceweb",
            "windent"});
            this.cmbBoxSelectPMS.Location = new System.Drawing.Point(77, 34);
            this.cmbBoxSelectPMS.Name = "cmbBoxSelectPMS";
            this.cmbBoxSelectPMS.Size = new System.Drawing.Size(304, 33);
            this.cmbBoxSelectPMS.TabIndex = 0;
            this.cmbBoxSelectPMS.SelectedIndexChanged += new System.EventHandler(this.cmbBoxSelectPMS_SelectedIndexChanged);
            // 
            // lblConnectionString
            // 
            this.lblConnectionString.AutoSize = true;
            this.lblConnectionString.Location = new System.Drawing.Point(77, 481);
            this.lblConnectionString.Name = "lblConnectionString";
            this.lblConnectionString.Size = new System.Drawing.Size(195, 25);
            this.lblConnectionString.TabIndex = 11;
            this.lblConnectionString.Text = "Connection String: ";
            // 
            // txtConnectionString
            // 
            this.txtConnectionString.Location = new System.Drawing.Point(278, 475);
            this.txtConnectionString.Name = "txtConnectionString";
            this.txtConnectionString.Size = new System.Drawing.Size(757, 31);
            this.txtConnectionString.TabIndex = 12;
            // 
            // lblServer
            // 
            this.lblServer.AutoSize = true;
            this.lblServer.Location = new System.Drawing.Point(77, 122);
            this.lblServer.Name = "lblServer";
            this.lblServer.Size = new System.Drawing.Size(87, 25);
            this.lblServer.TabIndex = 1;
            this.lblServer.Text = "Server: ";
            // 
            // lblDatabase
            // 
            this.lblDatabase.AutoSize = true;
            this.lblDatabase.Location = new System.Drawing.Point(77, 189);
            this.lblDatabase.Name = "lblDatabase";
            this.lblDatabase.Size = new System.Drawing.Size(116, 25);
            this.lblDatabase.TabIndex = 4;
            this.lblDatabase.Text = "Database: ";
            // 
            // lblUserId
            // 
            this.lblUserId.AutoSize = true;
            this.lblUserId.Location = new System.Drawing.Point(77, 253);
            this.lblUserId.Name = "lblUserId";
            this.lblUserId.Size = new System.Drawing.Size(92, 25);
            this.lblUserId.TabIndex = 7;
            this.lblUserId.Text = "User Id: ";
            // 
            // lblPassword
            // 
            this.lblPassword.AutoSize = true;
            this.lblPassword.Location = new System.Drawing.Point(77, 322);
            this.lblPassword.Name = "lblPassword";
            this.lblPassword.Size = new System.Drawing.Size(118, 25);
            this.lblPassword.TabIndex = 9;
            this.lblPassword.Text = "Password: ";
            // 
            // txtServer
            // 
            this.txtServer.Location = new System.Drawing.Point(221, 116);
            this.txtServer.Name = "txtServer";
            this.txtServer.Size = new System.Drawing.Size(593, 31);
            this.txtServer.TabIndex = 3;
            this.txtServer.MouseClick += new System.Windows.Forms.MouseEventHandler(this.txtField_MouseClick);
            this.txtServer.MouseDown += new System.Windows.Forms.MouseEventHandler(this.txtServer_MouseDown);
            // 
            // txtDatabase
            // 
            this.txtDatabase.Location = new System.Drawing.Point(221, 183);
            this.txtDatabase.Name = "txtDatabase";
            this.txtDatabase.Size = new System.Drawing.Size(593, 31);
            this.txtDatabase.TabIndex = 6;
            this.txtDatabase.MouseClick += new System.Windows.Forms.MouseEventHandler(this.txtField_MouseClick);
            this.txtDatabase.MouseDown += new System.Windows.Forms.MouseEventHandler(this.txtDatabase_MouseDown);
            // 
            // txtUserId
            // 
            this.txtUserId.Location = new System.Drawing.Point(221, 247);
            this.txtUserId.Name = "txtUserId";
            this.txtUserId.Size = new System.Drawing.Size(593, 31);
            this.txtUserId.TabIndex = 8;
            this.txtUserId.MouseClick += new System.Windows.Forms.MouseEventHandler(this.txtField_MouseClick);
            // 
            // txtPassword
            // 
            this.txtPassword.Location = new System.Drawing.Point(221, 316);
            this.txtPassword.Name = "txtPassword";
            this.txtPassword.Size = new System.Drawing.Size(593, 31);
            this.txtPassword.TabIndex = 10;
            this.txtPassword.MouseClick += new System.Windows.Forms.MouseEventHandler(this.txtField_MouseClick);
            // 
            // lblMessage
            // 
            this.lblMessage.AutoSize = true;
            this.lblMessage.Location = new System.Drawing.Point(499, 538);
            this.lblMessage.Name = "lblMessage";
            this.lblMessage.Size = new System.Drawing.Size(59, 25);
            this.lblMessage.TabIndex = 16;
            this.lblMessage.Text = "Error";
            // 
            // btnPopulateConnectionString
            // 
            this.btnPopulateConnectionString.Location = new System.Drawing.Point(374, 618);
            this.btnPopulateConnectionString.Name = "btnPopulateConnectionString";
            this.btnPopulateConnectionString.Size = new System.Drawing.Size(407, 51);
            this.btnPopulateConnectionString.TabIndex = 13;
            this.btnPopulateConnectionString.Text = "Populate Connection String";
            this.btnPopulateConnectionString.UseVisualStyleBackColor = true;
            this.btnPopulateConnectionString.Click += new System.EventHandler(this.btnPopulateConnectionString_Click);
            // 
            // lblStar1
            // 
            this.lblStar1.AutoSize = true;
            this.lblStar1.ForeColor = System.Drawing.Color.Red;
            this.lblStar1.Location = new System.Drawing.Point(170, 122);
            this.lblStar1.Name = "lblStar1";
            this.lblStar1.Size = new System.Drawing.Size(20, 25);
            this.lblStar1.TabIndex = 2;
            this.lblStar1.Text = "*";
            // 
            // lblStar2
            // 
            this.lblStar2.AutoSize = true;
            this.lblStar2.ForeColor = System.Drawing.Color.Red;
            this.lblStar2.Location = new System.Drawing.Point(195, 189);
            this.lblStar2.Name = "lblStar2";
            this.lblStar2.Size = new System.Drawing.Size(20, 25);
            this.lblStar2.TabIndex = 5;
            this.lblStar2.Text = "*";
            // 
            // btnEditConnectionString
            // 
            this.btnEditConnectionString.Location = new System.Drawing.Point(858, 618);
            this.btnEditConnectionString.Name = "btnEditConnectionString";
            this.btnEditConnectionString.Size = new System.Drawing.Size(291, 51);
            this.btnEditConnectionString.TabIndex = 15;
            this.btnEditConnectionString.Text = "Edit Connection String";
            this.btnEditConnectionString.UseVisualStyleBackColor = true;
            this.btnEditConnectionString.Click += new System.EventHandler(this.btnEditConnectionString_Click);
            // 
            // lnkSQLBrowser
            // 
            this.lnkSQLBrowser.AutoSize = true;
            this.lnkSQLBrowser.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lnkSQLBrowser.LinkColor = System.Drawing.Color.CornflowerBlue;
            this.lnkSQLBrowser.Location = new System.Drawing.Point(1075, 478);
            this.lnkSQLBrowser.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.lnkSQLBrowser.Name = "lnkSQLBrowser";
            this.lnkSQLBrowser.Size = new System.Drawing.Size(157, 29);
            this.lnkSQLBrowser.TabIndex = 17;
            this.lnkSQLBrowser.TabStop = true;
            this.lnkSQLBrowser.Text = "SQL Browser";
            this.lnkSQLBrowser.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.lnkSQLBrowser_LinkClicked);
            // 
            // FormDatabaseConnectionDebugger
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(12F, 25F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.ClientSize = new System.Drawing.Size(1273, 716);
            this.Controls.Add(this.lnkSQLBrowser);
            this.Controls.Add(this.btnEditConnectionString);
            this.Controls.Add(this.lblStar2);
            this.Controls.Add(this.lblStar1);
            this.Controls.Add(this.btnPopulateConnectionString);
            this.Controls.Add(this.lblMessage);
            this.Controls.Add(this.txtPassword);
            this.Controls.Add(this.txtUserId);
            this.Controls.Add(this.txtDatabase);
            this.Controls.Add(this.txtServer);
            this.Controls.Add(this.lblPassword);
            this.Controls.Add(this.lblUserId);
            this.Controls.Add(this.lblDatabase);
            this.Controls.Add(this.lblServer);
            this.Controls.Add(this.txtConnectionString);
            this.Controls.Add(this.lblConnectionString);
            this.Controls.Add(this.cmbBoxSelectPMS);
            this.Controls.Add(this.btnCheckDBConnection);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "FormDatabaseConnectionDebugger";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Database Connection Debugger";
            this.Load += new System.EventHandler(this.FormDatabaseConnectionDebugger_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnCheckDBConnection;
        private System.Windows.Forms.ComboBox cmbBoxSelectPMS;
        private System.Windows.Forms.Label lblConnectionString;
        private System.Windows.Forms.TextBox txtConnectionString;
        private System.Windows.Forms.Label lblServer;
        private System.Windows.Forms.Label lblDatabase;
        private System.Windows.Forms.Label lblUserId;
        private System.Windows.Forms.Label lblPassword;
        private System.Windows.Forms.TextBox txtServer;
        private System.Windows.Forms.TextBox txtDatabase;
        private System.Windows.Forms.TextBox txtUserId;
        private System.Windows.Forms.TextBox txtPassword;
        private System.Windows.Forms.Label lblMessage;
        private System.Windows.Forms.Button btnPopulateConnectionString;
        private System.Windows.Forms.Label lblStar1;
        private System.Windows.Forms.Label lblStar2;
        private System.Windows.Forms.Button btnEditConnectionString;
        private System.Windows.Forms.LinkLabel lnkSQLBrowser;
    }
}

